<?php

/**
 * Implementation of CCK's hook_widget_settings_alter().
 */
function field_inherit_widget_settings_alter(&$settings, $op, $widget) {
  
  // Altering the field settings form
  if ($op == 'form') {
    drupal_add_js(drupal_get_path('module', 'field_inherit') .'/field_inherit.js');
    $settings['field_inherit'] = array(
      '#type' => 'fieldset',
      '#title' => t('Field inherit'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 10,
    );
    $settings['field_inherit']['field_inherit'] = array(
      '#type' => 'checkbox',
      '#title' => t('Inherit this field from a parent node'),
      '#description' => t('If field is left empty, it will inherit a value from the same field of a parent node (as determined below).'),
      '#default_value' => (bool) $widget['field_inherit'],
    );
    // Determine sources of inherited values
    $options['menu'] = t('Parent menu item');
    if (module_exists('nodereference')) {
      $options['nodereference'] = t('Node Reference');
    }
    $settings['field_inherit']['field_inherit_source'] = array(
      '#type' => 'radios',
      '#title' => t('Source of inherited value'),
      '#options' => $options,
      '#default_value' => !empty($widget['field_inherit_source']) ? $widget['field_inherit_source'] : 'menu',
    );
    if (module_exists('nodereference')) {
      // TODO: there must be a better way to grab the content type?
      $type = str_replace('-', '_', arg(3));
      // Get list of Node Reference fields for this content type
      $options = array(NULL => t('<none>'));
      $results = db_query("SELECT field.field_name
        FROM {content_node_field} field
        LEFT JOIN {content_node_field_instance} instance ON field.field_name = instance.field_name
        WHERE field.type = 'nodereference' AND instance.type_name = '%s'
        ORDER BY field.field_name", $type);
      while ($row = db_fetch_array($results)) {
        $options[$row['field_name']] = $row['field_name'];
      }
      $settings['field_inherit']['field_inherit_nodereference'] = array(
        '#type' => 'select',
        '#title' => t('Node Reference field'),
        '#options' => $options,
        '#default_value' => (string) $widget['field_inherit_nodereference'],
      );
    }
    return $settings;
  }
  
  // Saving the field settings form
  if ($op == 'save') {
    $settings = array_merge($settings, array('field_inherit', 'field_inherit_source', 'field_inherit_nodereference'));
  }
  
}

/**
 * Implementation of hook_nodeapi().
 */
function field_inherit_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  // Viewing node
  if ($op == 'view') {
    $type = content_types($node->type);
    // Loop through all available fields
    foreach ($type['fields'] as $field) {
      // Field inheritance is enabled, and node's field is empty
      if (!empty($field['widget']['field_inherit']) and _field_inherit_is_empty($field, $node->$field['field_name'])) {
        // Look for parent node
        switch ($field['widget']['field_inherit_source']) {
          // Node Reference mode
          case 'nodereference':
            // Find the parent node
            $parent_node = _field_inherit_parent_nodereference($node, $field);
            break;
          // Menu mode
          case 'menu':
          default:
            // Node has a menu entry
            if ($menu_item = db_fetch_object(db_query("SELECT * FROM {menu_links} WHERE link_path = 'node/%d' ORDER BY mlid ASC", $node->nid))) {
              // Find the parent node
              $parent_node = _field_inherit_parent_menu($node, $field, $menu_item);
            }
            break;
        }
        // Found a parent node
        if (!empty($parent_node)) {
          // Prepare parent's field -- see content_view_field()
          $items = $parent_node->$field['field_name'];
          $parent_node->build_mode = isset($node->build_mode) ? $node->build_mode : NODE_BUILD_NORMAL;
          $function = $field['module'] .'_field';
          if (function_exists($function)) {
            $function('sanitize', $parent_node, $field, $items, $a3, $a4);
            $node->$field['field_name'] = $items;
          }
          $view = content_field('view', $parent_node, $field, $items, $a3, $a4);
          // Copy inherited field
          $node->content[$field['field_name']] = $view[$field['field_name']];
        }
      }
    }
  }
}

/**
 * Checks if field is empty -- see content_set_empty().
 */
function _field_inherit_is_empty($field, $items) {
  $empty = TRUE;
  $function = $field['module'] .'_content_is_empty';
  foreach ((array) $items as $delta => $item) {
    // Special handling for ImageField: if the image is the default, then it
    // is considered empty for this module
    if ($field['module'] == 'filefield' and $item['source'] == 'default_image_upload') {
      continue;
    }
    if (!$function($item, $field)) {
      $empty = FALSE;
    }
  }
  return $empty;
}

/**
 * Traverses up the menu trail to find a value to inherit.
 */
function _field_inherit_parent_menu($node, $field, $menu_item) {
  // Menu has a parent item
  if ($parent_menu_item = db_fetch_object(db_query("SELECT * FROM {menu_links} WHERE mlid = %d", $menu_item->plid))) {
    $path = explode('/', $parent_menu_item->link_path);
    // Parent is a node (but parent is not "itself" -- avoides infinite loop)
    if ($path[0] == 'node' and $path[1] != $node->nid) {
      $parent_node = node_load($path[1]);
      // Parent node has the requested field, and it's not empty
      if (isset($parent_node->{$field['field_name']}) and !_field_inherit_is_empty($field, $parent_node->$field['field_name'])) {
        return $parent_node;
      }
    }
    // No luck here; go up a level
    return _field_inherit_parent_menu($node, $field, $parent_menu_item);
  }
  return FALSE;
}

/**
 * Traverses up Node References to find a value to inherit.
 */
function _field_inherit_parent_nodereference($node, $field) {
  // Node doesn't reference itself (avoides infinite loop)
  if ($node->{$field['widget']['field_inherit_nodereference']}[0]['nid'] != $node->nid) {
    $parent_node = node_load($node->{$field['widget']['field_inherit_nodereference']}[0]['nid']);
    // Parent node has the requested field, and it's not empty
    if (isset($parent_node->$field['field_name']) and !_field_inherit_is_empty($field, $parent_node->$field['field_name'])) {
      return $parent_node;
    }
    // No luck here; if parent node references the another node via the same
    // nodereference field, then let's check it out
    if (!empty($parent_node->{$field['widget']['field_inherit_nodereference']}[0]['nid'])) {
      return _field_inherit_parent_nodereference($parent_node, $field);
    }
  }
  return FALSE;
}
