
Drupal.behaviors.field_inherit_settings_form = function(context) {
  
  // Toggle Node Reference field based on source of inherited value
  $('input[id^=edit-field-inherit-source]').change(function() {
    if ($('input[id^=edit-field-inherit-source]:checked').val() == 'nodereference') {
      $('#edit-field-inherit-nodereference-wrapper').show();
    } else {
      $('#edit-field-inherit-nodereference-wrapper').css('display', 'none');
    }
  }).triggerHandler('change');
  
};
